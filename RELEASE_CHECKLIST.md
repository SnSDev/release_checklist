# Release Checklist

- [ ] Verify all Cargo.toml versions updated
- [ ] Pass all tests
- [ ] Resolve all warnings
- [ ] Resolve all TODOs
- [ ] Update documentation
- [ ] Resolve all documentation warnings
- [ ] Review documentation
- [ ] Review `GOALS.md`
- [ ] Commit changes
- [ ] Squash all `[skip ci]` commits
- [ ] Tag version
- [ ] Install locally
- [ ] Verify manual tests locally
- [ ] Push `master`
- [ ] Ensure CI passes (n/a in this project)
- [ ] Push tag
